//Clé API
var keyApiFlickr = 'fdd28ced6c72683d796b3a2f05e3d681'

//Fonction nombre aleatoire
function random(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
};

//Action sur le bouton
var bouton = document.getElementById('bouton');
bouton.onclick = function (){
    var form = document.getElementById('formulaire').value;
    var a = random(0, 100);
    //Création requete http et conversion JSON
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key='+ keyApiFlickr +'&tags='+ form +'&format=json&nojsoncallback=1');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        var reponse = JSON.parse(xhr.responseText);

        //Pour obtenir une images : https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg";

        //parametre pour l'url
        var Id = reponse.photos.photo[a].id;
        var secretId = reponse.photos.photo[a].secret;
        var farmId = reponse.photos.photo[a].farm;
        var serverId = reponse.photos.photo[a].server;

        var urlPhoto = 'https://farm'+ farmId +'.staticflickr.com/'+ serverId +'/'+ Id +'_'+ secretId +'.jpg';

        //creation de l'image
        var div = document.getElementById("resultat");
        div.removeChild(div.childNodes[0]);
        var e = document.createElement("img");
        e.setAttribute('src', urlPhoto);
        div.appendChild(e);
      }
    };
    xhr.send();
};
